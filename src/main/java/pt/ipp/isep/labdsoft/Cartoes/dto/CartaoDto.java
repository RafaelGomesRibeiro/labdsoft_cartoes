package pt.ipp.isep.labdsoft.Cartoes.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pt.ipp.isep.labdsoft.Cartoes.domain.Cartao;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CartaoDto {
    public Long idCartao;
    public Long idUtente;

}
