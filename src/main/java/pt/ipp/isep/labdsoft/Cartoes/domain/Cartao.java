package pt.ipp.isep.labdsoft.Cartoes.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;

import javax.persistence.*;

@Entity
// @Table(name = "`candidaturas`", uniqueConstraints = {@UniqueConstraint(columnNames = {"cartaoCidadao"})})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Cartao {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(unique = true)
    Long idCartao;
    @Column(unique = true)
    Long idUtente;

    public Cartao(Long idCartao, Long idUtente) {
        if(idCartao < 0) throw new IllegalArgumentException("ID do cartão deve ser um numero não negativo");
        this.idCartao = idCartao;
        this.idUtente = idUtente;
    }

    public CartaoDto toDto(){
        return new CartaoDto(this.idCartao,this.idUtente);
    }
    public static Cartao fromDTO(CartaoDto c){
        if(c.idCartao < 0) throw new IllegalArgumentException("ID do cartão deve ser um numero não negativo");
        return new Cartao(c.idCartao,c.idUtente);
    }

}
