package pt.ipp.isep.labdsoft.Cartoes.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ValidaAtribuicaoServiceImpl implements ValidaAtribuicaoService {

    private final WebClient webClient;
    @Value("${UTENTE_BASE_URL}"+"/api/utente/exists/")
    private String urlValidacaoUtente;

    public ValidaAtribuicaoServiceImpl(){
        this.webClient = WebClient.create();
    }

    @Override
    public Boolean validaAtribuicaoServie(Long numPaciente) {
        return webClient.get().uri(urlValidacaoUtente+numPaciente).retrieve().bodyToMono(Boolean.class).block();
    }
}
