package pt.ipp.isep.labdsoft.Cartoes.dto;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CorredorDto {
    public String nome;
    public CorredorDto next;
    public GabineteDto gabinete;



}
