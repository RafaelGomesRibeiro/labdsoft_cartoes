package pt.ipp.isep.labdsoft.Cartoes.service;

import io.swagger.v3.core.util.Json;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import pt.ipp.isep.labdsoft.Cartoes.domain.Cartao;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.CorredorDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.Cartoes.persistence.CartaoRepository;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CartaoServiceImpl implements CartaoService {

    @Autowired
    private CartaoRepository cartaoRepo;
    @Autowired
    private ValidaAtribuicaoService validaService;

    private final WebClient webClient = WebClient.create();

    @Value("${GABINETE_BASE_URL}")
    private String gabinetesBaseURL;

    @Value("${UTENTE_BASE_URL}")
    private String usersBaseURL;

    @Override
    public List<Cartao> listAll() {
        return cartaoRepo.findAll();
    }

    @Override
    public Cartao createCartao(CartaoDto cartaoDto) {
        if (cartaoRepo.existsCartaoByIdCartao(cartaoDto.idCartao)) throw new DataIntegrityViolationException("");
        cartaoDto.idUtente = null;
        return cartaoRepo.save(Cartao.fromDTO(cartaoDto));

    }

    @Override
    public Cartao byIdCartao(Long idCartao) {
        return cartaoRepo.findCartaoByIdCartao(idCartao).orElseThrow(() -> new NoSuchElementException("Cartao com id " + idCartao + " não existe!"));
    }

    @Override
    public Cartao byIdUtente(Long idUtente) {
        return cartaoRepo.findCartaoByIdUtente(idUtente).orElseThrow(() -> new NoSuchElementException("Cartao nao atribuido a utente " + idUtente + "!"));
    }

    @Override
    public CartaoDto atribuirCartao(Long idCartao, Long idUtente) {
        if (validaService.validaAtribuicaoServie(idUtente)) {
            Cartao cartao = cartaoRepo.findCartaoByIdCartao(idCartao).orElseThrow(() -> new NoSuchElementException("Cartao com id " + idCartao + " não existe!"));
            if (cartao.getIdUtente() != null) throw new DataIntegrityViolationException("");
            cartao.setIdUtente(idUtente);
            return cartaoRepo.save(cartao).toDto();
        } else {
            throw new IllegalArgumentException("Utente Não Existente");
        }
    }

    @Override
    public List<String> chegadaUtente(Long idCartao) {
        CartaoDto cartao = byIdCartao(idCartao).toDto();
        if (cartao.idUtente == null)
            throw new NoSuchElementException("Cartão " + idCartao + " não tem nenhum utente associado!");
        //pedido aos gabinetes, para ver que gabinete se encontra disponivel

        String[] idGabinete = webClient.get().uri(gabinetesBaseURL + "/api/gabinetes/aOcupar/ " + cartao.idUtente)
                .retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    return clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                            msg -> {
                                if (msg.getStatusCode() == 404) {
                                    throw new NoSuchElementException(msg.getErrorMessage());
                                } else {
                                    throw new IllegalArgumentException(msg.getErrorMessage());
                                }
                            }
                    );

                })
                .bodyToMono(String[].class).block();
        if (idGabinete == null) return Arrays.asList(idGabinete);
        return Arrays.asList(idGabinete);
    }

    @Override
    public Boolean entradaUtente(Long iCartao, String idGabinete) {
        CartaoDto cartao = byIdCartao(iCartao).toDto();
        if (cartao.idUtente == null)
            throw new NoSuchElementException("Cartão " + iCartao + " não tem nenhum utente associado!");

        //pedido aos gabinetes, para ver que gabinete se encontra disponivel
        return pedidoEntradaSaida(cartao, idGabinete, "entrada");
    }

    @Override
    public Boolean saidaUtente(Long iCartao, String idGabinete) {
        CartaoDto cartao = byIdCartao(iCartao).toDto();
        if (cartao.idUtente == null)
            throw new NoSuchElementException("Cartão " + iCartao + " não tem nenhum utente associado!");

        //pedido aos gabinetes, para ver que gabinete se encontra disponivel
        return pedidoEntradaSaida(cartao, idGabinete, "saida");


    }

    @Override
    public List<Long> utentesSemCartao() {
        UtenteDTO[] utentes = webClient.get().uri(usersBaseURL + "/api/utente/").retrieve().bodyToMono(UtenteDTO[].class).block();
        List<Long> idUtentes = new ArrayList<>();
        for (UtenteDTO u : utentes) {
            idUtentes.add(u.numPaciente);
        }
        List<Cartao> cartoes = listAll();
        for (Cartao c : cartoes) {
            idUtentes.remove(c.getIdUtente());
        }
        return idUtentes;
    }

    @Override
    public CorredorDto corredorUtente(Long idCartao, String corredorId) {
        CartaoDto cartao = byIdCartao(idCartao).toDto();
        return webClient.get().uri(gabinetesBaseURL + "/api/planta/entrar/" + corredorId.trim() + "/" + cartao.idUtente.toString().trim())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    return clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                            msg -> {
                                if (msg.getStatusCode() == 404) {
                                    throw new NoSuchElementException(msg.getErrorMessage());
                                } else {
                                    throw new IllegalArgumentException(msg.getErrorMessage());
                                }
                            }
                    );

                }).bodyToMono(CorredorDto.class).block();
    }

    @Override
    public Boolean saidaClinicaUtente(Long cartaoId) {
        CartaoDto cartao = byIdCartao(cartaoId).toDto();

        return webClient.get().uri(gabinetesBaseURL + "/api/planta/saida/" + cartao.idUtente.toString().trim())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    return clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                            msg -> {
                                if (msg.getStatusCode() == 404) {
                                    throw new NoSuchElementException(msg.getErrorMessage());
                                } else {
                                    throw new IllegalArgumentException(msg.getErrorMessage());
                                }
                            }
                    );

                }).bodyToMono(Boolean.class).block();
    }

    private Boolean pedidoEntradaSaida(CartaoDto cartao, String idGabinete, String caminho) {
        return webClient.get().uri(gabinetesBaseURL + "/api/gabinetes/" + caminho.trim() + "/" + idGabinete.trim() + "/" + cartao.idUtente.toString().trim())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    return clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                            msg -> {
                                if (msg.getStatusCode() == 404) {
                                    throw new NoSuchElementException(msg.getErrorMessage());
                                } else {
                                    throw new IllegalArgumentException(msg.getErrorMessage());
                                }
                            }
                    );

                }).bodyToMono(Boolean.class).block();
    }


}
