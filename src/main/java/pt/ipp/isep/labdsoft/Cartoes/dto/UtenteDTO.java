package pt.ipp.isep.labdsoft.Cartoes.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class UtenteDTO {
    public Long numPaciente;
}
