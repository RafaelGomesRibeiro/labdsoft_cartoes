package pt.ipp.isep.labdsoft.Cartoes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Cartoes.domain.Cartao;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.CorredorDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.IdGabineteDTO;
import pt.ipp.isep.labdsoft.Cartoes.service.CartaoService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/")
    public ResponseEntity<List<CartaoDto>> cartoes(){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.listAll().stream().map(Cartao::toDto).collect(Collectors.toList()));
    }

    @GetMapping("/utentesSemCartao/")
    public ResponseEntity<List<Long>> utentesSemCartao(){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.utentesSemCartao());
    }

    @GetMapping("/cartao/{id}")
    public ResponseEntity<CartaoDto> cartaoById(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.byIdCartao(id).toDto());
    }
    @GetMapping("/utente/{id}")
    public ResponseEntity<CartaoDto> cartaoByIdUtente(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.byIdUtente(id).toDto());
    }

    @PostMapping("/")
    public ResponseEntity<CartaoDto> createCartao(@RequestBody CartaoDto cartao){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.createCartao(cartao).toDto());
    }

    @PutMapping("/{idCartao}/utente/{idUtente}")
    public ResponseEntity<CartaoDto> updateCartao(@PathVariable Long idCartao, @PathVariable Long idUtente){
        return ResponseEntity.status(HttpStatus.OK).body(cartaoService.atribuirCartao(idCartao, idUtente));
    }

    @PostMapping("/chegadaUtente/{idCartao}")
    public ResponseEntity<List<String>> chegadaUtente(@PathVariable Long idCartao){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(cartaoService.chegadaUtente(idCartao));
    }

    @PostMapping("/entradaGabinete/{idCartao}/{idGabinete}")
    public ResponseEntity<Boolean> entradaUtente(@PathVariable Long idCartao,@PathVariable String idGabinete){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(cartaoService.entradaUtente(idCartao,idGabinete));
    }

    @PostMapping("/saidaGabinete/{idCartao}/{idGabinete}")
    public ResponseEntity<Boolean> saidaUtente(@PathVariable Long idCartao, @PathVariable String idGabinete){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(cartaoService.saidaUtente(idCartao,idGabinete));
    }

    @PostMapping("/entradaCorredor/{idCartao}/{idCorredor}")
    public ResponseEntity<CorredorDto> entrarCorredor(@PathVariable String idCorredor, @PathVariable Long idCartao){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(cartaoService.corredorUtente(idCartao,idCorredor));
    }

    @GetMapping("/saida/{cartaoId}")
    public ResponseEntity<Boolean> saida(@PathVariable Long cartaoId){
        return new ResponseEntity<Boolean>(cartaoService.saidaClinicaUtente(cartaoId),HttpStatus.OK);
    }

}
