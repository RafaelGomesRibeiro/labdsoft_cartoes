package pt.ipp.isep.labdsoft.Cartoes.service;

import pt.ipp.isep.labdsoft.Cartoes.domain.Cartao;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;
import pt.ipp.isep.labdsoft.Cartoes.dto.CorredorDto;

import java.util.List;

public interface CartaoService {
    List<Cartao> listAll();
    Cartao createCartao(CartaoDto cartaoDto);
    Cartao byIdCartao(Long idCartao);
    Cartao byIdUtente(Long idUtente);
    CartaoDto atribuirCartao(Long idCartao, Long idUtente);
    List<String> chegadaUtente(Long idCartao);
    Boolean entradaUtente(Long iCartao, String idGabinete);
    Boolean saidaUtente(Long iCartao, String idGabinete);
    List<Long> utentesSemCartao();

    CorredorDto corredorUtente(Long idCartao, String corredorId);

    Boolean saidaClinicaUtente(Long cartaoId);
}
