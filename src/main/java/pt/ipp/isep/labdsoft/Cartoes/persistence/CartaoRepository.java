package pt.ipp.isep.labdsoft.Cartoes.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.labdsoft.Cartoes.domain.Cartao;

import java.util.Optional;

public interface CartaoRepository extends JpaRepository<Cartao,Long> {
    public Optional<Cartao> findCartaoByIdCartao(Long idCartao);
    public Optional<Cartao> findCartaoByIdUtente(Long idUtente);
    public boolean existsCartaoByIdCartao(Long idCartao);
}
