package pt.ipp.isep.labdsoft.Cartoes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IdGabineteDTO {
    public String gabineteId;
}
