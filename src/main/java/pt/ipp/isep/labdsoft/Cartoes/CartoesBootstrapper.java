package pt.ipp.isep.labdsoft.Cartoes;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Cartoes.controller.CartaoController;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;

@Component
public class CartoesBootstrapper implements ApplicationRunner {

    private CartaoController cartaoController;

    public CartoesBootstrapper(CartaoController cartaoController){
        this.cartaoController = cartaoController;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        registarCartao(6);
    }

    private void registarCartao(int quantity){
        for (int i = 0; i<quantity; i++){
            cartaoController.createCartao(CartaoDto.builder().idCartao(Long.parseLong(String.valueOf(i))).idUtente(null).build());
        }
    }

}
