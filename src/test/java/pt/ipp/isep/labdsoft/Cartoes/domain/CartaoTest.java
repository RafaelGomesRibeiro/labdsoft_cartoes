package pt.ipp.isep.labdsoft.Cartoes.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Cartoes.dto.CartaoDto;

@RunWith(SpringJUnit4ClassRunner.class)
public class CartaoTest {

    @Test
    public void createValidCartaoWithArgs(){
        Cartao cartao = new Cartao(12L,13L);
        Assertions.assertEquals(12L,cartao.getIdCartao());
        Assertions.assertEquals(13L,cartao.getIdUtente());

    }

    @Test
    public void createInvalidCartao(){
        try {
            Cartao cartao = new Cartao(-12L, 12L);
        }catch (Exception e){
            Assertions.assertEquals(e.getClass(),IllegalArgumentException.class);
            Assertions.assertEquals(e.getMessage(),"ID do cartão deve ser um numero não negativo");
        }

    }

    @Test
    public void toDtoTest(){
        CartaoDto dto = new Cartao(44L,55L).toDto();
        Assertions.assertEquals(44L,dto.idCartao);
        Assertions.assertEquals(55L,dto.idUtente);
    }

}
